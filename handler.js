const withdrawHandler = require("./SocketHandlers/withdraw");

module.exports = (socket) => {
  let socketuser = socket.request.user;
  // Socketuser is false if the user is not logged in
  if (socketuser.logged_in == false) socketuser = false;

  socket.on(
    "onsiteGiftcardWithdraw",
    withdrawHandler.onsiteGiftCardWithdraw(socketuser)
  );
};
