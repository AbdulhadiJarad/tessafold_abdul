const config = {
  withdraw: {
    minEarnedToWithdraw: 200,
    minEarnedToVerify: 100,
    allowedTypes: ["Fortnite", "Visa", "Amazon", "Steam", "Roblox"],
    allowedCountryCodes: ["US", "UK", "CA", "DE", "FR", "AU", "WW"],
  },
  events: {
    withdrawFailed: "withdrawFailed",
    withdrawSuccess: "withdrawSuccess",
    withdrawPending: "withdrawPending",
  },
};

module.exports = config;
