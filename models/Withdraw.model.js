const assert = require("better-assert");
const database = require("../database");
const { withdrawQueries } = require("../database/queries/withdraw.queries");
const assert = require("better-assert");

class WithdrawModel {
  getAccountStanding = async (gainid) => {
    const rows = await database.query(withdrawQueries.getAccountStanding, [
      gainid,
      gainid,
      gainid,
      gainid,
      gainid,
    ]);

    if (!rows) return { data: null, errorMessage: "" };

    let accountStanding = {
      banned: false,
      frozen: false,
      muted: false,
      countryBanned: false,
      deleted: false,
    };

    if (rows[0].length) accountStanding.banned = true;
    if (rows[1].length) accountStanding.frozen = true;
    if (rows[2].length) accountStanding.muted = true;
    if (rows[3].length) accountStanding.countryBanned = true;
    if (rows[4][0].deleted) accountStanding.deleted = true;

    return { data: accountStanding, errorMessage: null };
  };

  isDefaultUserEmailVerified = async (gainid) => {
    const rows = await database.query(
      withdrawQueries.isDefaultUserEmailVerified,
      [gainid]
    );

    if (!rows) return { data: null, errorMessage: "" };

    if ((rows && rows[0] && rows[0].email_confirmed) || !rows[0]) {
      return { data: true, errorMessage: null };
    }
    return { data: false, errorMessage: null };
  };

  getBalanceByGainId = async (gainid) => {
    const rows = await database.query(withdrawQueries.getBalanceByGainId, [
      gainid,
    ]);
    if (!rows) return { data: null, errorMessage: "" };

    assert(rows.length === 1);
    return { data: rows[0].balance, errorMessage: null };
  };

  earnedEnoughToWithdraw = async (gainid) => {
    const rows = await database.query(withdrawQueries.earnedEnoughToWithdraw, [
      gainid,
      gainid,
      gainid,
    ]);
    if (!rows) return { data: null, errorMessage: "" };

    if (rows[0].coins && rows[0].coins >= config.withdraw.minEarnedToWithdraw)
      return { data: true, errorMessage: null };

    return { data: false, errorMessage: null };
  };

  getUserVerifiedStatus = async (gainid) => {
    const rows = await database.query(withdrawQueries.getUserVerifiedStatus, [
      gainid,
    ]);
    if (!rows) return { data: null, errorMessage: "" };

    if (result.isVerified) return { data: true, errorMessage: null };

    return { data: false, errorMessage: null };
  };

  updateBalanceByGainId = async (gainid, amount) => {
    const rows = await database.query(withdrawQueries.updateBalanceByGainId, [
      amount,
      gainid,
    ]);
    if (!rows) return { data: null, errorMessage: "" };

    assert(rows.affectedRows === 1);

    const rows2 = await database.query(withdrawQueries.insertNewBalance, [
      gainid,
      amount,
      gainid,
    ]);

    if (!rows2) return { data: null, errorMessage: "" };

    assert(rows2.affectedRows === 1);
    return { data: true, errorMessage: null };
  };

  insertPendingSiteGiftCardWithdraw = async (
    gainid,
    coinAmount,
    cardType,
    countryCode,
    date,
    warningMessage
  ) => {
    const rows = await database.query(
      withdrawQueries.insertPendingSiteGiftCardWithdraw,
      [gainid, date, warningMessage, coinAmount, cardType, date, countryCode]
    );

    if (!rows) return { data: null, errorMessage: "" };

    assert(result.length === 2);
    return { data: rows, errorMessage: null };
  };

  insertSiteGiftCardWithdrawal = async ({
    gainid,
    coinAmount,
    cardCode,
    cardType,
    countryCode,
    date,
    approver,
  }) => {
    connection.query(withdrawQueries.insertSiteGiftCardWithdrawal, [
      gainid,
      date,
      approver,
      coinAmount,
      cardCode,
      cardType,
      date,
      countryCode,
    ]);

    if (!rows) return { data: null, errorMessage: "" };

    assert(result.length === 2);
    return { data: rows, errorMessage: null };
  };
}

module.exports = WithdrawModel;
