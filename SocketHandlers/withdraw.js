const withdrawGiftCard = require("../WithdrawService/withdraw.js");
const config = require("../configs/general.config");
const messages = require("../constants");

//Refactored onWithdraw event to a handler that calls the withdrawal service
const withdrawHandler = {
  onsiteGiftCardWithdraw: (socket, socketUser) => {
    return function (data) {
      //Check if user's logged in
      if (!socketUser) return feedback(`Please login to withdraw!`);

      if (!data) return feedback(`An unknown error occurred`);

      //Validate and get input
      let result = validateInput(data);
      if (result.errorMessage) {
        return feedback(result.errorMessage);
      }

      return withdrawGiftCard(
        socket,
        socketUser.gainId,
        result.data.type,
        result.countryCode,
        result.coinAmount
      );
    };
  },
};

//Input validation
const validateInput = (data) => {
  if (!data.type) {
    return {
      data: null,
      errorMessage: messages.serverError,
    };
  }

  if (isNaN(data.coinAmount) || !data.coinAmount) {
    return { data: null, errorMessage: `Please select an amount!` };
  }

  let type = data.type;

  let coinAmount;
  try {
    coinAmount = parseInt(data.coinAmount);
  } catch (error) {
    return {
      data: null,
      errorMessage: `An error occured. coinAmount is not a number.`,
    };
  }
  let countryCode = data.countryCode || "WW";

  if (
    !type ||
    !config.withdraw.allowedTypes.includes(type) ||
    !config.withdraw.allowedCountryCodes.includes(countryCode)
  ) {
    return {
      data: null,
      errorMessage: `An error occurred. Please try refreshing.`,
    };
  }

  return { data: { type, coinAmount, countryCode }, errorMessage: null };
};

module.exports = withdrawHandler;
