const WithdrawModel = require("../../models/Withdraw.model");
const config = require("../configs/general.config");

const withdrawModel = new WithdrawModel();

let minEarnText = `You must earn at least ${
  config.withdraw.minEarnedToWithdraw
} coins ($${(config.withdraw.minEarnedToWithdraw / 1000).toFixed(
  2
)}) through the offer walls before withdrawing.<br>This is to prevent abuse of the site bonuses. Please contact staff with any questions.`;

const withdrawGiftCard = async (
  socket,
  gainId,
  feedbackType,
  countryCode,
  coinAmount
) => {
  //Get account Status
  let accountStandingResult = await withdrawModel.getAccountStanding(gainId);

  if (accountStandingResult.data) {
    return socket.emit(
      config.events.withdrawFailed,
      accountStandingResult.errorMessage,
      feedbackType
    );
  }

  //If user is banned or their account is forzen, return failure.
  if (accountStandingResult.data.banned || accountStandingResult.data.frozen) {
    return socket.emit(
      config.events.withdrawFailed,
      `You are currently banned from withdrawing, please contact staff if you believe this is a mistake.`,
      feedbackType
    );
  }

  checkEmailVerification(gainId, socket);

  checkUserBalance(gainId, socket);

  checkCreditAmount(gainId, socket);

  // check if the user is verified (earned more than $100)
  let isUserVerifiedResult = await withdrawModel.getUserVerifiedStatus(gainId);

  if (isUserVerifiedResult.errorMessage) {
    return socket.emit(
      config.events.withdrawFailed,
      isUserVerifiedResult.errorMessage,
      feedbackType
    );
  }

  try {
    let { giftCardCode } = await GiftcardService.getGiftcard(
      feedbackType,
      coinAmount,
      countryCode
    );

    if (!isUserVerifiedResult) {
      return await withdrawForUnverifiedUser(socket, false, feedbackType);
    }
    if (giftCardCode) {
      return await withdrawForVerifiedUser(socket, gainId, feedbackType);
    }

    return await withdrawForVerifiedUser(
      socket,
      gainId,
      giftCardCode,
      true,
      feedbackType
    );
  } catch (err) {
    //   console.error(err);
    return socket.emit(config.events.withdrawFailed, err, feedbackType);
  }
};

const withdrawForUnverifiedUser = async (
  socket,
  gainId,
  outOfStock,
  feedbackType
) => {
  let result = await withdrawModel.updateBalanceByGainId(
    gainId,
    coinAmount * -1
  );

  if (result.errorMessage) {
    console.error(err);
    return socket.emit(
      config.events.withdrawFailed,
      `An error occurred, please try again`,
      feedbackType
    );
  }

  let insertionResult = await withdrawModel.insertPendingSiteGiftCardWithdraw(
    gainId,
    coinAmount,
    feedbackType,
    countryCode,
    utils.getIsoString(),
    null
  );

  if (insertionResult.errorMessage) {
    console.error(err);
    return socket.emit(
      config.events.withdrawFailed,
      `An error occurred, please try again`,
      feedbackType
    );
  }

  socket.emit("withdrawalPending", {
    coins: coinAmount,
  });

  Notifications.storeNotification(
    gainId,
    "Info",
    "pendingwithdrawal",
    `Your Gift Card withdrawal worth ${coinAmount} coins is pending.`
  );

  if (outOfStock) {
    socket.emit(
      config.events.withdrawSuccess,
      `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`,
      feedbackType
    );
  } else {
    socket.emit(
      config.events.withdrawPending,
      `Success!<br>Since you are not verified, a staff member has been notified and will review your withdrawal shortly! Check your Profile page to view your redemption code after the withdrawal has been approved.<br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`,
      feedbackType
    );
  }

  emitBalance(gainId);
};

const withdrawForVerifiedUser = async (
  socket,
  gainId,
  giftCardCode,
  outOfStock,
  feedbackType
) => {
  let result = await withdrawModel.updateBalanceByGainId(
    gainId,
    coinAmount * -1
  );

  if (result.errorMessage) {
    console.error(err);
    return socket.emit(
      config.events.withdrawFailed,
      `An error occurred, please try again`,
      feedbackType
    );
  }

  let insertionResult = await withdrawModel.insertSiteGiftCardWithdrawal({
    gainId,
    coinAmount,
    giftCardCode,
    feedbackType,
    countryCode,
    date: Date.now / 1000,
    approver: "Moderator",
  });

  if (insertionResult.errorMessage) {
    console.error(err);
    return socket.emit(
      config.events.withdrawFailed`An error occurred, please try again`,
      feedbackType
    );
  }

  socket.emit("withdrawalSuccessful", {
    giftCardCode,
  });

  Notifications.storeNotification(
    gainId,
    "Info",
    "withdrawalSuccessful",
    `Your ${feedbackType} Gift Card withdrawal worth ${coinAmount} coins has been successful.`
  );

  if (outOfStock) {
    socket.emit(
      config.events.withdrawSuccess,
      `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`
    );
  } else {
    socket.emit(
      config.events.withdrawSuccess,
      `Success!<br>You will receive your card shortly by email.`,
      feedbackType
    );
  }

  emitBalance(gainId);
};

const checkEmailVerification = async (gainId, socket) => {
  //Check for email verificaiton
  let isEmailVerifiedResult = await withdrawModel.isDefaultUserEmailVerified(
    gainId
  );

  if (isEmailVerifiedResult.errorMessage) {
    console.error(isEmailVerifiedResult.errorMessage);
    return socket.emit(
      config.events.withdrawFailed,
      isEmailVerifiedResult.errorMessage,
      feedbackType
    );
  }
  if (!isEmailVerifiedResult.data) {
    return socket.emit(
      config.events.withdrawFailed,
      `You must verify your E-mail address before requesting a withdrawal!`,
      feedbackType
    );
  }
};

const checkUserBalance = async (gainId, socket) => {
  //Check the user's balance
  let userBalanceResult = await withdrawModel.getBalanceByGainId(gainId);

  if (userBalanceResult.errorMessage) {
    return socket.emit(
      config.events.withdrawFailed,
      userBalanceResult.errorMessage,
      feedbackType
    );
  }

  //If not enough balance to complete the transaction, return failure
  if (userBalanceResult.data < coinAmount) {
    return socket.emit(
      config.events.withdrawFailed,
      `You don't have enough balance!`,
      feedbackType
    );
  }
};

const checkCreditAmount = async (gainId, socket) => {
  // check if user has earned enough credits ($200) to withdraw
  let hasEnoughCreditResult = await withdrawModel.earnedEnoughToWithdraw(
    gainId
  );

  if (hasEnoughCreditResult.errorMessage) {
    return socket.emit(
      config.events.withdrawFailed,
      hasEnoughCreditResult.errorMessage,
      feedbackType
    );
  }

  if (!hasEnoughCreditResult.data) {
    return socket.emit(config.events.withdrawFailed, minEarnText, feedbackType);
  }
};

module.exports = withdrawGiftCard;
